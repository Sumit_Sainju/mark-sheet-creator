package com.texas1.msc.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * MarkSheetDto
 * Created On : 5/11/2024 4:36 PM
 **/
@Getter
@Setter
public class MarkSheetDto {
    private Integer rollNumber;
    private String studentFullName;
    private Integer fullMarks;
    private Integer obtainedMarks;
    private String passed;
    private String failedSubjectsTheory;
    private float percentage;
}
