package com.texas1.msc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

/**
 * FileDto
 * Created On : 5/9/2024 6:56 AM
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileDto {
    private MultipartFile multipartFile;
}
