package com.texas1.msc.dto;

import com.poiji.annotation.ExcelCellName;
import lombok.Getter;
import lombok.Setter;

/**
 * StudentDetailsDto
 * Created On : 5/11/2024 1:21 PM
 **/
@Getter
@Setter
public class StudentDetailsDto {
    @ExcelCellName("Roll Number")
    private int rollNumber;

    @ExcelCellName("Student Full Name")
    private String studentFullName;

    @ExcelCellName("Computer Network Theory")
    private int computerNetworkTheory;

    @ExcelCellName("Computer Network Practical")
    private int computerNetworkPractical;

    @ExcelCellName("TOC Theory")
    private int tocTheory;

    @ExcelCellName("TOC Practical")
    private int tocPractical;

    @ExcelCellName("AI Theory")
    private int aiTheory;

    @ExcelCellName("AI Practical")
    private int aiPractical;

    @ExcelCellName("DBMS Theory")
    private int dbmsTheory;

    @ExcelCellName("DBMS Practical")
    private int dbmsPractical;

    @ExcelCellName("OS Theory")
    private int osTheory;

    @ExcelCellName("OS Practical")
    private int osPractical;
}
