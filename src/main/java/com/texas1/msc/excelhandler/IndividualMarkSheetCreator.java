package com.texas1.msc.excelhandler;

import com.texas1.msc.entity.MarkSheet;
import com.texas1.msc.entity.StudentDetails;
import com.texas1.msc.repo.MarkSheetRepo;
import com.texas1.msc.repo.StudentsDetailsRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;

/**
 * IndividualMarkSheetCreator
 * Created On : 5/12/2024 8:06 PM
 **/
@Slf4j
@Component
public class IndividualMarkSheetCreator {
    private final StudentsDetailsRepo studentsDetailsRepo;

    public IndividualMarkSheetCreator(StudentsDetailsRepo studentsDetailsRepo) {
        this.studentsDetailsRepo = studentsDetailsRepo;
    }

    public String individualMarkSheetCreator(Integer rollNumber) {
        for (StudentDetails studentDetails : studentsDetailsRepo.findAll()) {
            if (studentDetails.getRollNumber() == (rollNumber)) {
                excelCreator(studentDetails);
                return "Mark Sheet of " + studentDetails.getStudentFullName() + " Created";
            }
        }
        return "Details of Roll Number " + rollNumber + " not found";
    }

    public void excelCreator(StudentDetails studentDetails) {
        try{
            String filename = "D:/MarksheetOf" + studentDetails.getStudentFullName()+".xls";
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("0");
            HSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Name");
            row.createCell(1).setCellValue(studentDetails.getStudentFullName());

            HSSFRow row2 = sheet.createRow(1);
            row2.createCell(0).setCellValue("Subject");
            row2.createCell(1).setCellValue("Full Marks");
            row2.createCell(2).setCellValue("Pass Marks");
            row2.createCell(3).setCellValue("Obtained Marks");
            row2.createCell(4).setCellValue("Remarks");
            
            HSSFRow row3 = sheet.createRow(3);
            row3.createCell(0).setCellValue("Computer Network (Theory)");
            row3.createCell(1).setCellValue(60);
            row3.createCell(2).setCellValue(24);
            row3.createCell(2).setCellValue(studentDetails.getComputer_network_theory());
            row3.createCell(3).setCellValue(studentDetails.getComputer_network_theory() >= 24? "Pass" : "Failed");

            HSSFRow row4 = sheet.createRow(4);
            row4.createCell(0).setCellValue("Computer Network (Practical)");
            row4.createCell(1).setCellValue(40);
            row4.createCell(2).setCellValue(16);
            row4.createCell(2).setCellValue(studentDetails.getComputer_network_practical());
            row4.createCell(3).setCellValue(studentDetails.getComputer_network_theory() >= 16? "Pass" : "Failed");

            HSSFRow row5 = sheet.createRow(5);
            row5.createCell(0).setCellValue("AI (Theory)");
            row5.createCell(1).setCellValue(60);
            row5.createCell(2).setCellValue(24);
            row5.createCell(2).setCellValue(studentDetails.getAi_theory());
            row5.createCell(3).setCellValue(studentDetails.getAi_theory() >= 24? "Pass" : "Failed");

            HSSFRow row6 = sheet.createRow(6);
            row6.createCell(0).setCellValue("AI (Practical)");
            row6.createCell(1).setCellValue(40);
            row6.createCell(2).setCellValue(16);
            row6.createCell(2).setCellValue(studentDetails.getAi_theory());
            row6.createCell(3).setCellValue(studentDetails.getAi_practical() >= 16? "Pass" : "Failed");

            HSSFRow row7 = sheet.createRow(7);
            row7.createCell(0).setCellValue("OS (Theory)");
            row7.createCell(1).setCellValue(60);
            row7.createCell(2).setCellValue(24);
            row7.createCell(2).setCellValue(studentDetails.getOs_theory());
            row7.createCell(3).setCellValue(studentDetails.getOs_theory() >= 24? "Pass" : "Failed");

            HSSFRow row8 = sheet.createRow(8);
            row8.createCell(0).setCellValue("OS (Practical)");
            row8.createCell(1).setCellValue(40);
            row8.createCell(2).setCellValue(16);
            row8.createCell(2).setCellValue(studentDetails.getAi_practical());
            row8.createCell(3).setCellValue(studentDetails.getOs_practical() >= 16? "Pass" : "Failed");

            HSSFRow row9 = sheet.createRow(9);
            row9.createCell(0).setCellValue("TOC (Theory)");
            row9.createCell(1).setCellValue(60);
            row9.createCell(2).setCellValue(24);
            row9.createCell(2).setCellValue(studentDetails.getToc_theory());
            row9.createCell(3).setCellValue(studentDetails.getToc_theory() >= 24? "Pass" : "Failed");

            HSSFRow row10 = sheet.createRow(10);
            row10.createCell(0).setCellValue("TOC (Practical)");
            row10.createCell(1).setCellValue(40);
            row10.createCell(2).setCellValue(16);
            row10.createCell(2).setCellValue(studentDetails.getToc_practical());
            row10.createCell(3).setCellValue(studentDetails.getToc_practical() >= 16? "Pass" : "Failed");

            HSSFRow row11 = sheet.createRow(11);
            row11.createCell(0).setCellValue("DBMS (Theory)");
            row11.createCell(1).setCellValue(60);
            row11.createCell(2).setCellValue(24);
            row11.createCell(2).setCellValue(studentDetails.getDbms_theory());
            row11.createCell(3).setCellValue(studentDetails.getDbms_theory() >= 24? "Pass" : "Failed");

            HSSFRow row12 = sheet.createRow(12);
            row12.createCell(0).setCellValue("DMBS (Practical)");
            row12.createCell(1).setCellValue(40);
            row12.createCell(2).setCellValue(16);
            row12.createCell(2).setCellValue(studentDetails.getDbms_practical());
            row12.createCell(3).setCellValue(studentDetails.getDbms_practical() >= 16? "Pass" : "Failed");

            FileOutputStream fileOut = new FileOutputStream(filename);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
            log.info("Mark Sheet of " + studentDetails.getStudentFullName() + " Created");
        } catch (Exception e) {
            log.info("Mark Sheet of " + studentDetails.getStudentFullName() + " Creation Failed");
        }
    }
}
