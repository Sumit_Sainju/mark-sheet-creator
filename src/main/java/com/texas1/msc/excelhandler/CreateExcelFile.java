package com.texas1.msc.excelhandler;

import com.texas1.msc.entity.MarkSheet;
import com.texas1.msc.repo.MarkSheetRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.tokens.ScalarToken;

import java.io.FileOutputStream;
import java.util.List;

/**
 * CreateExcelFile
 * Created On : 5/11/2024 7:49 PM
 **/
@Slf4j
@Component
public class CreateExcelFile{
    public void convertIntoExcel(MarkSheetRepo markSheetRepo){
        try {
            String filename = "D:/MarkSheet.xls";
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("FirstSheet");
            short rowNumber = 0;
            HSSFRow rowHead = sheet.createRow(rowNumber);
            rowHead.createCell(0).setCellValue("Roll Number");
            rowHead.createCell(1).setCellValue("Student Name");
            rowHead.createCell(2).setCellValue("Full Marks");
            rowHead.createCell(3).setCellValue("Obtained Marks");
            rowHead.createCell(4).setCellValue("Passed");
            rowHead.createCell(5).setCellValue("Failed Subjects Theory");
            rowHead.createCell(6).setCellValue("Failed Subjects Practical");
            rowHead.createCell(7).setCellValue("Percentage");
            rowNumber++;

            List<MarkSheet> markSheetList = markSheetRepo.findAll();
            for(MarkSheet markSheet : markSheetList) {
                HSSFRow row = sheet.createRow(rowNumber);
                row.createCell(0).setCellValue(markSheet.getRollNumber());
                row.createCell(1).setCellValue(markSheet.getStudentFullName());
                row.createCell(2).setCellValue(markSheet.getFullMarks());
                row.createCell(3).setCellValue(markSheet.getObtainedMarks());
                row.createCell(4).setCellValue(markSheet.getPassed());
                row.createCell(5).setCellValue(markSheet.getFailedSubjectsTheory());
                row.createCell(6).setCellValue(markSheet.getFailedSubjectsPractical());
                row.createCell(7).setCellValue(markSheet.getPercentage());
                rowNumber++;
            }

            FileOutputStream fileOut = new FileOutputStream(filename);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
            log.info("Excel File has been generated!!!");

        } catch(Exception e) {
            log.info("Error occured");
        }
    }

}
