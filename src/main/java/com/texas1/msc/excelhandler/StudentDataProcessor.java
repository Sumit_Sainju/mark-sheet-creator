package com.texas1.msc.excelhandler;

import com.texas1.msc.dto.MarkSheetDto;
import com.texas1.msc.dto.StudentDetailsDto;
import com.texas1.msc.entity.MarkSheet;
import com.texas1.msc.entity.StudentDetails;
import com.texas1.msc.repo.MarkSheetRepo;
import com.texas1.msc.repo.StudentsDetailsRepo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * StudentDataProcessor
 * Created On : 5/11/2024 1:33 PM
 **/
@Slf4j
@Component
public class StudentDataProcessor {
    private final StudentsDetailsRepo studentsDetailsRepo;
    private final MarkSheetRepo markSheetRepo;
    private final CreateExcelFile createExcelFile;

    public StudentDataProcessor(StudentsDetailsRepo studentsDetailsRepo, MarkSheetRepo markSheetRepo,
                                CreateExcelFile createExcelFile) {
        this.studentsDetailsRepo = studentsDetailsRepo;
        this.markSheetRepo = markSheetRepo;
        this.createExcelFile = createExcelFile;
    }

    public void processStudentDetailsDto(List<StudentDetailsDto> studentDetailsDtoList) {
        List<StudentDetailsDto> studentDetailsDtoList1 = studentDetailsDtoList.stream().toList();
//        List<StudentDetails> allExistingStudents = studentsDetailsRepo.findAll();
//        List<Integer> existingRollNumbers = new ArrayList<>();
//        for(StudentDetails studentDetails : allExistingStudents ) {
//            existingRollNumbers.add(allExistingStudents.getRollNumber())
//        }
//        List<Integer> existingRollNumber = studentsDetailsRepo.findAllByRollNumber();
//        List<StudentDetailsDto> studentDetailsDtoList1 =
//                studentDetailsDtoList.stream().filter(StudentDetailsDto -> !existingRollNumber.contains
//                (StudentDetailsDto.getRollNumber())).collect(Collectors.toList());
//        List<StudentDetails> studentDetailsList = new ArrayList<>();
//        for (StudentDetailsDto studentDetailsDto : studentDetailsDtoList1) {
//            studentDetailsList.add(new StudentDetails(null, studentDetailsDto.getRollNumber(),
//                    studentDetailsDto.getStudentFullName(), studentDetailsDto.getComputerNetworkTheory(),
//                    studentDetailsDto.getComputerNetworkPractical(), studentDetailsDto.getTocTheory(),
//                    studentDetailsDto.getTocPractical(), studentDetailsDto.getAiTheory(),
//                    studentDetailsDto.getAiPractical(), studentDetailsDto.getDbmsTheory(),
//                    studentDetailsDto.getDbmsPractical(), studentDetailsDto.getOsTheory(),
//                    studentDetailsDto.getOsPractical()));
//        }

        for (StudentDetailsDto studentDetailsDto : studentDetailsDtoList1) {
            if (studentDetailsDto.getRollNumber() != 0) {
                try {
                    studentsDetailsRepo.save(new StudentDetails(null, studentDetailsDto.getRollNumber(),
                            studentDetailsDto.getStudentFullName(), studentDetailsDto.getComputerNetworkTheory(),
                            studentDetailsDto.getComputerNetworkPractical(), studentDetailsDto.getTocTheory(),
                            studentDetailsDto.getTocPractical(), studentDetailsDto.getAiTheory(),
                            studentDetailsDto.getAiPractical(), studentDetailsDto.getDbmsTheory(),
                            studentDetailsDto.getDbmsPractical(), studentDetailsDto.getOsTheory(),
                            studentDetailsDto.getOsPractical()));
                } catch (Exception exception) {
                    log.info("Roll Number already exist!!!");
                }
            }
        }

//        for (StudentDetails studentDetails : studentDetailsList) {
//            try {
////                studentsDetailsRepo.saveAll(studentDetailsList);
//                studentsDetailsRepo.save(studentDetails);
//            } catch (Exception exception) {
//                log.info("Roll Number already exist!!!");
//            }
//        }
        createMarkSheet();
        createExcelFile.convertIntoExcel(markSheetRepo);
    }

    public void createMarkSheet() {
        List<StudentDetails> studentDetailsList = studentsDetailsRepo.findAll();
//        List<MarkSheetDto> markSheetDtoList = new ArrayList<>();
        for (StudentDetails studentDetails : studentDetailsList) {
            if (studentDetails.getRollNumber() != 0) {
                try {
                    int obtainedMarks = CalculateObtainedMarks(studentDetails);
                    int fullMarks = 800;
                    String passed = "No";
                    String failedSubjectTheory = FindFailedSubjectTheory(studentDetails);
                    String failedSubjectPractical = FIndFailedSubjectPractical(studentDetails);
                    if (failedSubjectPractical.isEmpty() && failedSubjectTheory.isEmpty()) {
                        passed = "Yes";
                    }
                    float percentage = ((float) obtainedMarks / fullMarks) * 100;

                    markSheetRepo.save(new MarkSheet(null, studentDetails.getRollNumber(),
                            studentDetails.getStudentFullName(), fullMarks, obtainedMarks, passed,
                            failedSubjectTheory, failedSubjectPractical, percentage));
                } catch (Exception e) {
                    log.info("Already in the database!!!");
                }
            }
        }
    }

    public Integer CalculateObtainedMarks(StudentDetails studentDetails) {
        return studentDetails.getComputer_network_theory() + studentDetails.getComputer_network_practical() +
                studentDetails.getToc_theory() + studentDetails.getToc_practical() + studentDetails.getAi_theory() +
                studentDetails.getAi_practical() + studentDetails.getDbms_theory() +
                studentDetails.getDbms_practical() + studentDetails.getOs_theory() + studentDetails.getOs_practical();
    }

    public String FindFailedSubjectTheory(StudentDetails studentDetails) {
        StringBuilder stringBuilder = new StringBuilder();
        int failedSubject = 0;
        if (studentDetails.getComputer_network_theory() < 24) {
            stringBuilder.append("Computer Network");
            failedSubject++;
        }
        if (studentDetails.getToc_theory() < 24) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("TOC");
            failedSubject++;
        }
        if (studentDetails.getAi_theory() < 24) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("AI");
            failedSubject++;
        }
        if (studentDetails.getDbms_theory() < 24) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("DBMS");
            failedSubject++;
        }
        if (studentDetails.getOs_theory() < 24) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("OS");
        }
        return stringBuilder.toString();
    }

    public String FIndFailedSubjectPractical(StudentDetails studentDetails) {
        StringBuilder stringBuilder = new StringBuilder();
        int failedSubject = 0;
        if (studentDetails.getComputer_network_practical() < 16) {
            stringBuilder.append("Computer Network");
            failedSubject++;
        }
        if (studentDetails.getToc_practical() < 16) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("TOC");
            failedSubject++;
        }
        if (studentDetails.getAi_practical() < 16) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("AI");
            failedSubject++;
        }
        if (studentDetails.getDbms_practical() < 16) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("DBMS");
            failedSubject++;
        }
        if (studentDetails.getOs_practical() < 16) {
            if (failedSubject > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("OS");
        }
        return stringBuilder.toString();
    }
}
