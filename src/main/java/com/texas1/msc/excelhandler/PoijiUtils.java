package com.texas1.msc.excelhandler;

import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import com.poiji.option.PoijiOptions;
import com.texas1.msc.dto.StudentDetailsDto;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * PoijiUtils
 * Created On : 5/11/2024 1:16 PM
 **/
@Component
public class PoijiUtils {
    public List<StudentDetailsDto> readData(MultipartFile multipartFile) throws IOException {
        InputStream inputStream = multipartFile.getInputStream();
        PoijiOptions options = PoijiOptions.PoijiOptionsBuilder.settings().sheetIndex(2).build();
        List<StudentDetailsDto> studentDetailsDtoList = Poiji.fromExcel(inputStream, PoijiExcelType.XLSX, StudentDetailsDto.class, options);
        return studentDetailsDtoList;
    }
}
