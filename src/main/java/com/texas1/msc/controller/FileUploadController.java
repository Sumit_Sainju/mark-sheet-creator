package com.texas1.msc.controller;

import com.texas1.msc.dto.FileDto;
import com.texas1.msc.dto.StudentDetailsDto;
import com.texas1.msc.excelhandler.PoijiUtils;
import com.texas1.msc.excelhandler.StudentDataProcessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

/**
 * FileUploadController
 * Created On : 5/11/2024 1:13 PM
 **/
@Controller
@RequestMapping("/upload")
public class FileUploadController {
    private final PoijiUtils poijiUtils;
    private final StudentDataProcessor studentDataProcessor;

    public FileUploadController(PoijiUtils poijiUtils, StudentDataProcessor studentDataProcessor) {
        this.poijiUtils = poijiUtils;
        this.studentDataProcessor = studentDataProcessor;
    }

    @PostMapping("/excel")
    public String uploadExcel(@ModelAttribute FileDto fileDto) throws IOException {
        List<StudentDetailsDto> studentDetailsDtoList = poijiUtils.readData(fileDto.getMultipartFile());
        studentDetailsDtoList.stream().forEach(System.out::println);
        studentDataProcessor.processStudentDetailsDto(studentDetailsDtoList);
        return "redirect:/";
    }
}
