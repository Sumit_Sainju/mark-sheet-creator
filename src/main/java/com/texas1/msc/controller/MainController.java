package com.texas1.msc.controller;

import com.texas1.msc.dto.FileDto;
import com.texas1.msc.excelhandler.PoijiUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * MainController
 * Created On : 5/9/2024 6:54 AM
 **/
@Controller
@RequestMapping("/")
public class MainController {
    private final PoijiUtils poijiUtils;

    public MainController(PoijiUtils poijiUtils) {
        this.poijiUtils = poijiUtils;
    }

    @GetMapping
    public String openLandingPage(Model model){
        model.addAttribute("fileDto", new FileDto());
        return "landingpage";
    }
}
