package com.texas1.msc.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * StudentsDetails
 * Created On : 5/11/2024 1:45 PM
 **/
@Getter
@Setter
@Entity
@Table(name = "student_details", uniqueConstraints =
        {@UniqueConstraint(name = "uk_student_details_rollNumber", columnNames = "rollNumber")})
@NoArgsConstructor
@AllArgsConstructor
public class StudentDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private int rollNumber;

    @Column(name = "name", length = 30)
    private String studentFullName;

    private Integer computer_network_theory;
    private Integer computer_network_practical;
    private Integer toc_theory;
    private Integer toc_practical;
    private Integer ai_theory;
    private Integer ai_practical;
    private Integer dbms_theory;
    private Integer dbms_practical;
    private Integer os_theory;
    private Integer os_practical;



}
