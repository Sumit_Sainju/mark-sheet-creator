package com.texas1.msc.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * MarkSheet
 * Created On : 5/11/2024 4:47 PM
 **/
@Getter
@Setter
@Entity
@Table(name = "mark_sheet", uniqueConstraints = @UniqueConstraint(name = "uk_mark_sheet_roll_number", columnNames = "roll_number"))
@NoArgsConstructor
@AllArgsConstructor
public class MarkSheet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "roll_number")
    private Integer rollNumber;

    @Column(name = "student_name", length = 30)
    private String studentFullName;

    @Column(name = "full_marks")
    private Integer fullMarks;

    @Column(name = "obtained_marks")
    private Integer obtainedMarks;

    @Column(name = "passed", length = 3)
    private String passed;

    @Column(name = "failed_subjects_theory")
    private String failedSubjectsTheory;

    @Column(name = "failed_subjects_practical")
    private String failedSubjectsPractical;

    @Column(name = "percentage")
    private Float percentage;
}
