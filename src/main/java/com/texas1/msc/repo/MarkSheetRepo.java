package com.texas1.msc.repo;

import com.texas1.msc.entity.MarkSheet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarkSheetRepo extends JpaRepository<MarkSheet, Integer> {
}
