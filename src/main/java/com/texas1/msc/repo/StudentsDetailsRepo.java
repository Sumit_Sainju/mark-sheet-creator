package com.texas1.msc.repo;

import com.texas1.msc.entity.StudentDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentsDetailsRepo extends JpaRepository<StudentDetails, Integer> {
//    List<Integer> findAllByRollNumber();
}
