package com.texas1.msc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarkSheetCreatorApplication {

	public static void main(String[] args) {

		SpringApplication.run(MarkSheetCreatorApplication.class, args);
	}

}
